1. Директории
1.1. Products
В данной директории расположены основные компоненты системы "Базы Данных - Basic", "Система - PizzaAssistaceSystem", "Сайт - FastHotPizza"
1.1.1. DataDase>BasicPAS
Расположены две папки 
	* "DATA_sql" - расположены скрипты на создание базы данных "BasicPAS", создания таблиц и их данных;
	* "scripts" - расположены скрипты для разработки.
1.1.2. PizzaAssistanceSystem
Тут распологается основная часть системы.
1.1.2.1. data
В папке находятся исполняемые файлы ".py" ".pyw", главный файл для запуска системы "main_window.pyw"
1.1.2.1.1. Interface files
В папке расположены изображения и иконки системы
1.1.2.2. Отчеты
В папке сохраняются отчёты. Отчеты делает система в соответствующем разделе.
1.1.2.3. settingDataBase.txt
Файл с настройкой базы данных. Хранятся данные пользователя сервера.
1.1.3. siteFastHotPizza
---> Находится в разработке
1.2. Документы
В данной директории расположена Документация системы

2. Модули/библиотеки
Для успешного запуска системы необходимы:
	* pip install pymysql
	* pip install cryptography
	* pip install PyQt5
	* pip install PyQt5 pyside2
	* pip install xlsxwriter
	* pip install numpy

3. База данных
Используется в качестве базы данных: MySQL, MySQL Server
3.1. Общая информация
Необходимо установить: 
	* MySQL Server не ниже 8 версии;
	* MySQL MySQL Workbench 8.0 CE
	* Доп. MySQL Notifier
3.2. Создать локальный сервер 
3.3. Импорт базы данных
Из папки DataDase>BasicPAS>DATA_sql ипортировать через Workbench базу данных

4. Запуск системы
Исполняемый файл PizzaAssistanceSystem>data>main_window.pyw
4.1. Настройка базы данных 
Необходимо ввести логин и пароль в PizzaAssistanceSystem>settingDataBase.txt
4.2. Информация в консоли

В консоль могут выскочить следующие тексты:

	* Unknown property transform
	* Unknown property box-sizing
	* QObject::connect: cannot queue arguments of type 'qvector<int>' 
	  (make sure 'qvector<int>' is registered using qregistermetatype().)...

Данные уведомления не влияют на работу системы, по карйне мере на Windows
1-2 уведомления связаные с CSS, ругается на кнопки
3 уведомление, сам не знаю, на работу системы не влияет. В интернете мало информации по данному уведомлению, или я плохо ищу.












